using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UnitCreepTower : UnitTower {
	
	// Use this for initialization
	public void Start () {
		
		thisObj.layer=LayerManager.LayerCreepTower();

		baseStat.buildDuration=0;
		InitTower(BuildManager.PrePlaceTower());
	}
	
}