
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;


//using System.Collections.Generic;

public class UI : MonoBehaviour {
	public static bool bossTimer = false;
	public string server = "http://bit.sparcs.org:20001/";

	private string towers;

	public static int sellV;
	public enum _BuildMenuType{Fixed, Box, Pie}
	public _BuildMenuType buildMenuType;
	
	public enum _BuildMode{PointNBuild, DragNDrop}
	public _BuildMode buildMode;
	
	public bool showBuildSample=true;
	
	public int fastForwardSpeed=3;
	
	public bool enableTargetPrioritySwitch=true;
	
	public bool alwaysEnableNextButton=true;
	
	public string nextLevel="";
	public string selectStage="StageSceneNew";
	
	string[] labelShow=new string[10];
	float[] labelHeight = new float[10];
	int labelCnt;
	
	private bool enableSpawnButton=true;
	
	private bool buildMenu=false;
	private bool posToolTip;
	
	private UnitTower currentSelectedTower;
	
	private int[] currentBuildList=new int[0];
	private int[] currentTowerList=new int[40];
	private int[] currentTowerCnt = new int[40];
	private int[] currentMixCnt = new int[40];
	private int currentTowerListLength;
	
	int[,] fusion = new int[40, 40];
	
	//indicate if the player have won or lost the game
	private bool winLostFlag=false;
	
	private bool paused=false;
	
	
	private int w;
	private int h;
	
	GUIStyle smallFont;
	GUIStyle largeFont;
	bool fusionButtonClick;
	bool randomButtonClick;
	
	private int[] mixList;
	private int mixListLength;
	
	IEnumerator LabelShow(string a)
	{
		int i = labelCnt;
		labelShow[labelCnt++] = a;
		if (labelCnt == 5)
			labelCnt = 0;
		yield return new WaitForSeconds(1f);
		labelShow[i] = "";
		labelHeight[i] = h - h/10 - h/40;
		
		Debug.Log (labelCnt.ToString ());
	}
	
	private static UI ui;
	void Awake(){
		ui=this;
	}
	
	private string gameMessage="";
	private float lastMsgTime=0;
	public static void ShowMessage(string msg){
		ui.gameMessage=ui.gameMessage+msg+"\n";
		ui.lastMsgTime=Time.time;
	}
	IEnumerator MessageCountDown(){
		while(true){
			if(gameMessage!=""){
				while(Time.time-lastMsgTime<3){
					yield return null;
				}
				gameMessage="";
			}
			yield return null;
		}
	}
	
	// Use this for initialization
	void Start () {
      
      UnitTower[] towerList=BuildManager.GetTowerList();
      currentTowerListLength = towerList.Length;
      for (int i= 0; i<currentTowerListLength; i++) {
         currentTowerList [i] = i;
         currentTowerCnt [i] = 0;
         currentMixCnt [i] =0;
      }

      WWW www = new WWW (server + "getTowers/" + StartGUI.id );
      StartCoroutine(WaitForGetTower(www));
      Debug.Log (towers);

      w = Screen.width;
      h = Screen.height;
   ///   Debug.Log ("start");
      bossTimer = false;
      if (h > w){
         int temp = h;
         h = w;
         w = temp;
      }

      nextLevel=Application.loadedLevelName;
      
      labelCnt = 0;
      for (int i=0; i<5; i++) {
         labelShow[i] = "";
         labelHeight[i] = h - h/10 - h/40;
      }
      
      for (int i = 0; i< 40; i++){
         for (int j = 0; j<40; j++){
            fusion[i,j] = 0;
         }
      }
      //fusion list
		fusion[ 5, 0] = 2;
		fusion[ 6, 1] = 2;
		fusion[ 7, 2] = 2;
		fusion[ 8, 3] = 2;
		fusion[ 9, 0] = 1; fusion[ 9, 1] = 1; fusion[ 9, 2] = 1; fusion[ 9, 4] = 2;
		fusion[10, 0] = 1; fusion[10, 3] = 1; fusion[10, 5] = 2; fusion[10, 8] = 1;
		fusion[11, 1] = 1; fusion[11, 2] = 1; fusion[11, 6] = 3;
		fusion[12, 2] = 1; fusion[12, 6] = 1; fusion[12, 7] = 3;
		fusion[13, 3] = 2; fusion[13, 5] = 1; fusion[13, 8] = 2;
		fusion[14, 0] = 1; fusion[14, 8] = 1; fusion[14, 9] = 2;
		fusion[15, 4] = 1; fusion[15,10] = 2;
		fusion[16, 2] = 1; fusion[16,11] = 2;
		fusion[17, 0] = 1; fusion[17, 2] = 1; fusion[17, 7] = 1; fusion[17, 12] = 1;
		fusion[18, 2] = 1; fusion[18,13] = 1; fusion[18,14] = 1;
		fusion[19,10] = 1; fusion[19,14] = 1;
		fusion[20,15] = 2; fusion[20,19] = 1;
		fusion[21,16] = 2; fusion[21,19] = 1;
		fusion[22,15] = 1; fusion[22,16] = 1; fusion[22,17] = 1;
		fusion[23,16] = 1; fusion[23,18] = 2;
		fusion[24,16] = 1; fusion[24,18] = 1; fusion[24,19] = 1;
		fusion[25, 0] = 1; fusion[25, 5] = 1; fusion[25,10] = 1; fusion[25,15] = 1; fusion[25,20] = 2;
		fusion[26,12] = 1; fusion[26,15] = 1; fusion[26,21] = 1; fusion[26,24] = 1;
		fusion[27,16] = 1; fusion[27,17] = 1; fusion[27,22] = 2;
		fusion[28,22] = 1; fusion[28,23] = 1; fusion[28,24] = 1;
		fusion[29,10] = 1; fusion[29,13] = 1; fusion[29,24] = 2;
		fusion[30,25] = 1; fusion[30,26] = 1; fusion[30,27] = 1; fusion[30,28] = 1; fusion[30,29] = 1; 
      mixList = new int[40];
      mixListLength = 0;
      
      
      fusionButtonClick = false;
      randomButtonClick = false;
      
      smallFont = new GUIStyle();
      largeFont = new GUIStyle();
      
      smallFont.fontSize = h/30;
      largeFont.fontSize = h/20;
      
      smallFont.normal.textColor = Color.white;
      largeFont.normal.textColor = Color.white;


      //init the rect to be used for drawing ui box
      topPanelRect=new Rect(-3, -3, Screen.width+6, h/10);
      
      bottomPanelRect=new Rect(-3, Screen.height-h/20-5, Screen.width+6, h/10);
      
      //init ui rect for DragNDrop mode, this will be use throughout the entire game
      if(buildMode==_BuildMode.DragNDrop){
         UnitTower[] fullTowerList=BuildManager.GetTowerList();
         int width=50;
         int height=50;
         
         int x=0;
         int y=Screen.height-height-6-(int)bottomPanelRect.height;
         int menuLength=(fullTowerList.Length)*(width+3);
         
         buildListRect=new Rect(x, y, menuLength+3, height+6);
      }
      
      //this two lines are now obsolete
      //initiate sample menu, so player can preview the tower in pointNBuild buildphase
      //if(buildMode==_BuildMode.PointNBuild && showBuildSample) BuildManager.InitiateSampleTower();
      
      StartCoroutine(MessageCountDown());
   }
	
	
	void OnEnable(){
		SpawnManager.onClearForSpawningE += OnClearForSpawning;
		GameControl.onGameOverE += OnGameOver;
		UnitCreep.onBossE += OnGameOver;
		UnitTower.onDestroyE += OnTowerDestroy;
	}
	
	void OnDisable(){
		SpawnManager.onClearForSpawningE -= OnClearForSpawning;
		GameControl.onGameOverE -= OnGameOver;
		UnitCreep.onBossE -= OnGameOver;
		UnitTower.onDestroyE -= OnTowerDestroy;
	}
	
	//called when game is over, flag passed indicate user win or lost
	void OnGameOver(bool flag){
		bossTimer=false;
		winLostFlag=flag;
	}
	
	//called if a tower is unbuilt or destroyed, check to see if that tower is selected, if so, clear the selection
	void OnTowerDestroy(UnitTower tower){
		if(currentSelectedTower==tower) currentSelectedTower=null;
	}
	
	//caleed when SpawnManager clearFor Spawing event is detected, enable spawnButton
	void OnClearForSpawning(bool flag){
		enableSpawnButton=flag;
	}
	
	//call to enable/disable pause
	void TogglePause(){
		paused=!paused;
		if(paused){
			Time.timeScale=0;
			
			//close all the button, so user can interact with game when it's paused
			if(currentSelectedTower!=null){
				GameControl.ClearSelection();
				currentSelectedTower=null;
			}
			if(buildMenu){
				buildMenu=false;
				BuildManager.ClearBuildPoint();
			}
		}
		else Time.timeScale=1;
	}
	
	// Update is called once per frame
	void Update () {
		#if !UNITY_IPHONE && !UNITY_ANDROID
		if(buildMode==_BuildMode.PointNBuild)
			BuildManager.SetIndicator(Input.mousePosition);
		#endif
		
		if(Input.GetMouseButtonUp(0) && !IsCursorOnUI(Input.mousePosition) && !paused){
			fusionButtonClick = false;
			//check if user click on towers
			UnitTower tower=GameControl.Select(Input.mousePosition);

			
			//if user click on tower, select the tower
			if(tower!=null){

				//Debug.Log (tower.GetTowerID().ToString());


				currentSelectedTower=tower;
				
				//if build mode is active, disable buildmode
				if(buildMenu){
					buildMenu=false;
					BuildManager.ClearBuildPoint();
					ClearBuildListRect();
				}
			}
			//no tower is selected
			else{
				//if a tower is selected previously, clear the selection
				if(currentSelectedTower!=null){
					GameControl.ClearSelection();
					currentSelectedTower=null;
					towerUIRect=new Rect(0, 0, 0, 0);
					//UIRect.RemoveRect(towerUIRect);
				}
				
				//if we are in PointNBuild Mode
				if(buildMode==_BuildMode.PointNBuild){
					
					//check for build point, if true initiate build menu
					if(BuildManager.CheckBuildPoint(Input.mousePosition) && !fusionButtonClick){
						UpdateBuildList();
						InitBuildListRect();
						buildMenu=true;
					}
					//if there are no valid build point but we are in build mode, disable it
					else{
						if(buildMenu){
							buildMenu=false;
							BuildManager.ClearBuildPoint();
							ClearBuildListRect();
						}
					}
					
				}
			}
			
		}
		//if right click, 
		else if(Input.GetMouseButtonUp(1)){
			fusionButtonClick = false;
			//clear the menu
			if(buildMenu){
				buildMenu=false;
				BuildManager.ClearBuildPoint();
				ClearBuildListRect();
			}
			
			//if there are tower currently being selected
			if(currentSelectedTower!=null){
				CheckForTarget();
			}
		}
		
		//if escape key is pressed, toggle pause
		if(Input.GetKeyUp(KeyCode.Escape)){
			TogglePause();
		}


	}
	
	
	//check if user has click on creep, if yes and if current tower is eligible to attack it, set the assign it as tower target
	void CheckForTarget(){
		currentSelectedTower.CheckForTarget(Input.mousePosition);
		//~ Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		//~ RaycastHit hit;
		//~ LayerMask mask=currentSelectedTower.GetTargetMask();
		//~ if(Physics.Raycast(ray, out hit, Mathf.Infinity, mask)){
		//~ Unit unit=hit.collider.gameObject.GetComponent<Unit>();
		//~ if(unit!=null){
		//~ currentSelectedTower.AssignTarget(unit);
		//~ }
		//~ }
	}
	
	
	private Rect topPanelRect=new Rect(-3, -3, Screen.width+6, 28);
	private Rect bottomPanelRect;
	private Rect buildListRect;
	private Rect towerUIRect;
	private Rect[] scatteredRectList=new Rect[0];	//for piemenu
	
	//check for all UI screen space, see if user cursor is within any of them, return true if yes
	//this is to prevent user from being able to interact with in game object even when clicking on UI panel and buttons
	public bool IsCursorOnUI(Vector3 point){
		Rect tempRect=new Rect(0, 0, 0, 0);
		
		tempRect=topPanelRect;
		tempRect.y=Screen.height-tempRect.y-tempRect.height;
		if(tempRect.Contains(point)) return true;
		
		/*tempRect=bottomPanelRect;
		tempRect.y=Screen.height-tempRect.y-tempRect.height;
		if(tempRect.Contains(point)) return true;*/
		
		tempRect=buildListRect;
		tempRect.y=Screen.height-tempRect.y-tempRect.height;
		if(tempRect.Contains(point)) return true;
		
		tempRect=towerUIRect;
		tempRect.y=Screen.height-tempRect.y-tempRect.height;
		if(tempRect.Contains(point)) return true;
		
		if (fusionButtonClick){
			tempRect=new Rect(w-(w/100*2+h/10*6+24), h/10, h/10*6+24+w/100, h/10*7+24);
			tempRect.y=Screen.height-tempRect.y-tempRect.height;
			if (tempRect.Contains(point)) return true;
		}
		
		for(int i=0; i<scatteredRectList.Length; i++){
			tempRect=scatteredRectList[i];
			tempRect.y=Screen.height-tempRect.y-tempRect.height;
			if(tempRect.Contains(point)) return true;
		}
		
		return false;
	}
	
	//clear all ui space occupied by build menu
	void ClearBuildListRect(){
		if(buildMode==_BuildMode.PointNBuild){
			if(buildMenuType==_BuildMenuType.Pie) scatteredRectList=new Rect[0];
			else buildListRect=new Rect(0, 0, 0, 0);
		}
	}
	
	//initiate ui space that will be occupied by build menu
	void InitBuildListRect(){
		if(buildMode==_BuildMode.PointNBuild){
			if(buildMenuType==_BuildMenuType.Fixed){
				int width=50;
				int height=50;
				
				int x=0;
				int y=Screen.height-height-6-(int)bottomPanelRect.height;
				
				int menuLength=(currentBuildList.Length+1)*(width+3);
				
				//calculate the buildlist rect
				buildListRect=new Rect(x, y, menuLength+3, height+6);
			}
			else if(buildMenuType==_BuildMenuType.Box){
				//since this is a floating menu, the actual location cannot be pre-calculated
				//instead it will be calculated in every frame in OnGUI()
			}
			else if(buildMenuType==_BuildMenuType.Pie){
				//since this is a floating menu, the actual location cannot be pre-calculated
				//instead it will be calculated in every frame in OnGUI()
			}
		}
		else if(buildMode==_BuildMode.DragNDrop){
			//DragNDrop mode will always have build menu enable, so no need to calculate it
		}
	}
	
	//calculate the position occupied by each individual button based on number of button, position on screen and button size
	public Vector2[] GetPieMenuPos(int num, Vector3 pos, int size){
		//first off calculate the radius require to accomodate all buttons
		float radius=(num*size*1.8f)/(2*Mathf.PI);
		
		//create the rect array and the angle spacing required for the number of button
		Vector2[] piePos=new Vector2[num];
		float angle=200/(Mathf.Max(1, num-1));
		
		//loop through and calculate the button's position
		for(int i=0; i<num; i++){
			float x = pos.x+radius*Mathf.Sin((80)*Mathf.Deg2Rad+i*angle*Mathf.PI/180);
			float y = pos.y+radius*-Mathf.Cos((80)*Mathf.Deg2Rad+i*angle*Mathf.PI/180);
			
			piePos[i]=new Vector2(x, y);
		}
		
		return piePos;
	}
	
	string fixSize(string a, string h){
		return "<size="+h+">" + a + "</size>";
	}
	//draw GUI
	public GUISkin skin;
	void OnGUI(){
		GUI.depth=100;
		GUI.skin=skin;
		int bSize = h / 25;
		if (w < h) {
			bSize = w / 40;
		}
		string bSize_ = bSize.ToString ();
		string a = "";
		
		//general infobox
		//draw top panel
		GUI.BeginGroup (topPanelRect);
		for(int i=0; i<2; i++) GUI.Box(new Rect(0, 0, topPanelRect.width, topPanelRect.height), "");
		
		int buttonX=8;
		
		//if not pause, draw the spawnButton and fastForwardButton
		if(!paused){
			//if SpawnManager is ready to spawn
			if(enableSpawnButton){
				if(GUI.Button(new Rect(buttonX, 5, w/10, h/10 - 8), fixSize ("Spawn", bSize_))){
					//if the game is not ended
					if(GameControl.gameState!=_GameState.Ended){
						//if spawn is successful, disable the spawnButton
						if(SpawnManager.Spawn())
							enableSpawnButton=false;
					}
				}
				buttonX+=w/10 + 5;
			}
			
			//display the fastforward button based on current time scale
			if(Time.timeScale==1){
				
				if(GUI.Button(new Rect(buttonX, 5, w/10, h/10 - 8), fixSize (a="Timex" +fastForwardSpeed.ToString(), bSize_))){
					Time.timeScale=fastForwardSpeed;
				}
			}
			else{
				if(GUI.Button(new Rect(buttonX, 5, w/10, h/10 - 8), fixSize (a="Timex1", bSize_))){
					Time.timeScale=1;
				}
			}
		}
		//shift the cursor to where the next element will be drawn
		else buttonX+=w/10+5;
		
		//get all the resource information
		int resource=GameControl.GetResourceVal();
		float subStartX=10; float subStartY=0;
		
		buttonX = w / 2 - (w / 10 + w / 10 + w / 10 +w/40 )/2;
		GUI.Label(new Rect(buttonX, h/30-2, w/5, h/10 - 8), fixSize( "Money: "+resource.ToString(), bSize_) );
		buttonX += w / 10 + w / 120;
		GUI.Label(new Rect(buttonX, h/30-2, w/5, h/10 - 8), fixSize("  Life: "+GameControl.GetPlayerLife(), bSize_));
		buttonX += w / 10 + w / 120;
		GUI.Label(new Rect(buttonX, h/30-2, w/5, h/10 - 8), fixSize("Wave: "+SpawnManager.GetCurrentWave()+"/"+SpawnManager.GetTotalWave(), bSize_));
		
		//pause button
		if(GUI.Button(new Rect(topPanelRect.width-w/20-5, 5, w/20, h/10 - 8), fixSize("II", bSize_))) {
			TogglePause();
		}
		
		buttonX = w - w / 20 - w / 10 - w/8 - 10;
		if (GUI.Button (new Rect (buttonX, 5, w / 10, h / 10 - 8), fixSize("Bag", bSize_))) {
			if (fusionButtonClick){
				fusionButtonClick = false;
				//FusionMenuFix();
				//Debug.Log("bbbbb");
			}else{
				if(buildMenu){
					buildMenu=false;
					BuildManager.ClearBuildPoint();
					ClearBuildListRect();
				}
				fusionButtonClick = true;
				//FusionMenuFix();
				//Debug.Log("aaaa");
				
			}
		}
		buttonX+=w/10+5;
		if (GUI.Button (new Rect (buttonX, 5, w / 8, h / 10 - 8), fixSize("GetTower", bSize_))) {
			UnitTower[] towerList=BuildManager.GetTowerList();
			int num=Random.Range(0, 11);

			switch(num){
			case 0: case 1:	case 2:
				num = 0;
				break;
			case 3:	case 4:	case 5:
				num = 1;
				break;
			case 6:	case 7:
				num = 2;
				break;
			case 8: case 9:
				num = 3;
				break;
			case 10:
				num = 4;
				break;
			}

			for (int i=0; i<currentTowerListLength; i++){
				if(GameControl.HaveSufficientResource(1)){
					if (currentTowerList[i] == num){
						StartCoroutine(LabelShow(" You Get <color=red>" + towerList[num].unitName + "</color>!!"));
						GameControl.SpendResource(1);
						currentTowerCnt[i]++;
						currentMixCnt[i]++;
						break;
					}
				}else{
					StartCoroutine(LabelShow(" You Don't Have <color=red>Gold</color>"));
					break;
				}
			}
		}
		
		GUI.EndGroup ();
		
		
		if (fusionButtonClick && !paused) {
			FusionMenuFix(true);
			if(GUI.tooltip!=""){
				//Debug.Log(GUI.tooltip);
				int ID=int.Parse(GUI.tooltip);
				//if bag postooltip = true;
				posToolTip = true;
				ShowToolTip(ID);
			}
		}
		
		//draw bottom panel
		/*
		GUI.BeginGroup (bottomPanelRect);
			for(int i=0; i<2; i++) GUI.Box(new Rect(0, 0, bottomPanelRect.width, bottomPanelRect.height), "");
		
			
			subStartX+=70;
		GUI.EndGroup ();
		*/


		
		//if game is still not over
		if(GameControl.gameState!=_GameState.Ended){
			
			//clear tooltip, each button when hovered will assign tooltip with corresponding ID
			//the tooltip will be checked later, if there's anything, we show the corresponding tooltip
			GUI.tooltip="";
			
			//build menu
			if(buildMode==_BuildMode.PointNBuild){
				//if PointNBuild mode, only show menu when there are active buildpoint (build menu on)
				if(buildMenu){
					fusionButtonClick = false;
					if(buildMenuType==_BuildMenuType.Fixed) FusionMenuFix(false);
					else if(buildMenuType==_BuildMenuType.Box) BuildMenuBox();
					else if(buildMenuType==_BuildMenuType.Pie) BuildMenuPie();
				}
				//if there's no build menu, clear the buildList rect
				else buildListRect=new Rect(0, 0, 0, 0);
			}
			else if(buildMode==_BuildMode.DragNDrop){
				//if DragNDrop mode, show menu all time
				BuildMenuAllTowersFix();
			}
			
			//if the cursor is hover on top of tower button, the tooltip will bear the ID of the Buildable Tower
			if(GUI.tooltip!=""){
				int ID=int.Parse(GUI.tooltip);
			//	Debug.Log(GUI.tooltip);
				//show the build tooltip
				//if tile postooltip = false;
				posToolTip = false;
				ShowToolTip(ID);
				//if no preview has been showing, preview the sample tower on the grid, only needed in PointNBuild mode
				if(buildMode==_BuildMode.PointNBuild && showBuildSample) BuildManager.ShowSampleTower(ID); 
			}
			else{
				//if a sample tower is shown on the grid, only needed in PointNBuild mode
				if(buildMode==_BuildMode.PointNBuild && showBuildSample) BuildManager.ClearSampleTower();
			}
			
			//selected tower information UI
			/*
			if (currentSelectedTower != null){
				Debug.Log ("bbbb");
			}
*/
			if(currentSelectedTower!=null && !fusionButtonClick){
				SelectedTowerUI();
			}
			//else towerUIRect=new Rect(0, 0, 0, 0);
			
			//if paused, draw the pause menu
			if(paused){
				float ww = (float)w;
				float hh = (float)h;
				float startX=Screen.width/2-ww/14;
				float startY=Screen.height*0.35f;
				
				float fsize = h/32;
				string fsize_ = fsize.ToString ();
				
				for(int i=0; i<4; i++) GUI.Box(new Rect(startX, startY, ww/7, hh * 3/ 10), "Game Paused");
				
				startX+=(w/7-w/8)/2;
				startY+=hh/15;
				if(GUI.Button(new Rect(startX, startY, ww/8, hh/16), fixSize("Resume Game", fsize_))){
					TogglePause();
				}
				startY+=hh/16+5;
				if(GUI.Button(new Rect(startX, startY, ww/8, hh/16), fixSize("Restart Level", fsize_))){
					Application.LoadLevel(Application.loadedLevelName);
				}
				startY+=hh/16+5;
				if(GUI.Button(new Rect(startX, startY, ww/8, hh/16), fixSize("Select Stage", fsize_))){
					Debug.Log(selectStage);
					Application.LoadLevel ("StageSceneNew");
				}
			}
			
		}
		//gameOver, draw the game over screen
		else{
			float ww = (float)w;
			float hh = (float)h;
			float startX=Screen.width/2-ww/14;
			float startY=Screen.height*0.35f;
			
			float fsize = h/32;
			string fsize_ = fsize.ToString ();
			
			string levelCompleteString="Level Complete";
			if(!winLostFlag) levelCompleteString="Level Lost";
			
			for(int i=0; i<4; i++) GUI.Box(new Rect(startX, startY, ww/7, hh * 3/ 10), levelCompleteString);
			
			startX+=(w/7-w/8)/2;
			startY+=hh/15;
			
			if(GUI.Button(new Rect(startX, startY, ww/8, hh/16), fixSize("Restart Level", fsize_))){
				Application.LoadLevel(Application.loadedLevelName);
			}
			startY+=hh/16+5;
			if(alwaysEnableNextButton || winLostFlag){
				if(GUI.Button(new Rect(startX, startY, ww/8, hh/16), fixSize("Next Level", fsize_))){
					//if(nextLevel!="") Application.LoadLevel(nextLevel);
				}
			}
			startY+=hh/16+5;
			if(GUI.Button(new Rect(startX, startY, ww/8, hh/16), fixSize("Select Stage", fsize_))){
            if(selectStage!=""){
               if (winLostFlag){
                  Debug.Log (StageGUI.lastClear);
                  Debug.Log (StageGUI.stage);
                  if (StageGUI.lastClear < StageGUI.stage){
                     string url = server + "clear/" + StartGUI.id + "/" + StageGUI.stage.ToString();
                     WWW www = new WWW (url);
                     StartCoroutine (WaitForClear(www));
                     if (StageGUI.lastClear+1 == StageGUI.stage){
                        string temp = "";
                        for (int i=0; i<currentTowerListLength-1; i++){
                           temp += currentTowerCnt[i].ToString() + ",";
                        }
                        temp+=currentTowerCnt[currentTowerListLength-1].ToString ();
                        url = server + "towers/" + StartGUI.id + "/" + temp;
                        www = new WWW (url);
                        StartCoroutine(WaitForClear(www));
                     }
                  }
               }
					Application.LoadLevel ("StageSceneNew");
				}
			}
			
		}
		
		
		//if game message is not empty, show the game message.
		//shift the text alignment to LowerRight first then back to UpperLeft after the message
		if(gameMessage!=""){
			GUI.skin.label.alignment=TextAnchor.LowerRight;
			GUI.Label(new Rect(0, 0, Screen.width-5, Screen.height+12), gameMessage);
			GUI.skin.label.alignment=TextAnchor.UpperLeft;
		}
		
		
		for (int i =0; i<5; i++) 
		{
			if (labelShow[i] != ""){
				//Debug.Log (labelHeight[i].ToString ());
				int h_ = h/20;
				GUI.Label (new Rect (w / 30, labelHeight[i], w, h), "<size="+h_.ToString()+">" + labelShow[i] + "</size>");
				labelHeight[i] -= Time.deltaTime*60;
			}
		}


		int hhh = h/20;
		if (bossTimer){
			double leftTime = System.Math.Round(UnitCreep.bossLeftTime, 2);
			GUI.Label (new Rect(w/30, h/2, w, h),  "<size="+hhh.ToString()+">"+"Boss Time : " + leftTime.ToString()  + "</size>");
		}
		
	}
	
	private IEnumerator WaitForGetTower(WWW www){
      yield return www;
      if (www.error == null){
         towers = www.text;
         if (towers != ""){
            string[] towers_ = towers.Split (',');
            int i = 0;
            foreach (string word in towers_) {
               currentTowerCnt[i]+= System.Convert.ToInt32(word);
               currentMixCnt[i]+=System.Convert.ToInt32(word);
               i++;
            }
         }
         Debug.Log ("WWW Ok!: " + www.text);
      }else{
         Debug.Log ("WWW Error: " + www.error);
      }
   }
	
	private IEnumerator WaitForClear(WWW www){
		yield return www;
		if (www.error == null){
			Debug.Log ("WWW Ok!: " + www.text);
		}else{
			Debug.Log ("WWW Error: " + www.error);
		}
	}
	
	int fusionTower(){
		int i, j;
		int[] arr = new int[40];
		int cnt=0;

		if (mixListLength == 0)
			return -1;

		System.Array.Copy(mixList, 0, arr, 0, mixListLength);
		System.Array.Sort(arr, 0, mixListLength);
		j = -1;
		
		bool check = false;
		for (i=0; i<40; i++){
			cnt = 0;
			check = true;
			for (j=0; j<40; j++){
				for (int k=0; k<fusion[i,j]; k++){
					if (cnt == mixListLength){
						check = false;
						break;
					}
					if (j==arr[cnt]){
						cnt++;
					}else{
						check = false;
						break;
					}
				}
			}
			if (check && cnt == mixListLength) return i;
		}
		return -1;
	}
	void FusionMenuFix(bool fusion){
		int width=h/10;
		int height=h/10;
		
		int x=w-(w/100+h/10*5+18);
		int y=h/10;
		
		for(int i=0; i<3; i++) GUI.Box(new Rect(w-(w/100+h/10*5+18), h/10, h/10*5+18, h/10*7+24), "");
		//show up the build buttons, scrolling through currentBuildList initiated whevenever the menu is first brought up
		UnitTower[] towerList=BuildManager.GetTowerList();
		
		x+=3;	y+=3;
		
		for(int num=0; num<currentTowerListLength; num++){
			UnitTower tower=towerList[currentTowerList[num]];
			GUIContent guiContent; 
			if (currentMixCnt[num] > 0){
				guiContent=new GUIContent(tower.icon, num.ToString()); 
			}else{
				guiContent=new GUIContent((Texture)Resources.Load("Title&Icon/" + "Tower/Tower" + (num).ToString() +"Empty"),  num.ToString() ); 
				
			}
			if(GUI.Button(new Rect(x, y, width, height), guiContent)){
				if (fusion && mixListLength < 6 && currentMixCnt[num] > 0){
					mixList[mixListLength++] = num;
					currentMixCnt[num]--;
					currentTowerCnt[num]--;
				}
				if (!fusion){
					if (currentMixCnt[num] > 0){
						currentMixCnt[num]--;
						tower.buildID = currentTowerList[num];
						if(BuildButtonPressed(tower)){
							return;
						}
						
					}
				}
			}
			int font = h/30;
			GUI.Label(new Rect(x, y+height-h/25, h/10, h/20), "<size="+font.ToString ()+">x" + currentMixCnt[num].ToString() + "</size>" );
			
			x+=width+3;
			if ((num % 5)==4){
				x = w-(w/100+h/10*5+18) + 3;
				y += height + 3;
			}
		}
		if (fusion) MixMenuFix();
	}
	void MixMenuFix(){
		int width=h/10;
		int height=h/10;
		int x=w-(w/100*2+h/10*6+24);
		int y=h/10;
		
		for(int i=0; i<3; i++) GUI.Box(new Rect(w-(w/100*2+h/10*6+24), h/10, h/10+6, h/10*7+24), "");
		//show up the build buttons, scrolling through currentBuildList initiated whevenever the menu is first brought up
		
		x+=3;	y+=3;
		UnitTower[] towerList=BuildManager.GetTowerList();
		for(int num=0; num<mixListLength; num++){
			//	Debug.Log("babasda");
			//	int ID=currentBuildList[num];
			
			UnitTower tower=towerList[mixList[num]];
			
			GUIContent guiContent=new GUIContent(tower.icon, num.ToString()); 
			if(GUI.Button(new Rect(x, y, width, height), guiContent)){
				//if building was successful, break the loop can close the panel
				//if(BuildButtonPressed(tower)) return;
				mixListLength--;
				currentMixCnt[mixList[num]]++;
				currentTowerCnt[mixList[num]]++;
				for (int i = num; i<mixListLength; i++){
					mixList[i] = mixList[i+1];
				}
			}
			
			y+=height+3;
			
			
		}
		for (int i = mixListLength; i<6; i++) {
			GUIContent guiContent=new GUIContent(); 
			GUI.Button(new Rect(x, y, width, height), guiContent);
			y+=height+3;
		}
		
		if(GUI.Button(new Rect(x, y, width, height), "F")){
			int num = fusionTower();
			if (num >= 0){
				Debug.Log("fusion!");
				StartCoroutine(LabelShow(" Fusion Success!!"));
				/*
            for (int i=0; i<mixListLength; i++){
               if (currentTowerCnt[i] == 0){
                  Debug.Log("current build Tower : ");
                  Debug.Log(BuildManager.buildTowerCnt);
                  for (int j =0; j < BuildManager.buildTowerCnt; j++){
                     Debug.Log("buildTower Id: ");
                     Debug.Log(BuildManager.buildTowers[j].GetTowerID());
                  }
                  for (int j =0; j < BuildManager.buildTowerCnt; j++){
                     Debug.Log("buildTower Id: ");
                     Debug.Log(BuildManager.buildTowers[j].GetTowerID());
                     
                     Debug.Log("i: ");
                     Debug.Log(i);
                     
                     if (BuildManager.buildTowers[j].GetTowerID() == currentTowerList[i]){
                     //   BuildManager.buildTowers[j].SetActive();
                        BuildManager.buildTowers[j].Delete();
                        for (int k=j; k<BuildManager.buildTowerCnt-1; k++){
                           BuildManager.buildTowers[j] = BuildManager.buildTowers[j+1];
                        }
                        BuildManager.buildTowerCnt--;
                        break;
                     }
                  }
               }
               else currentTowerCnt[mixList[i]]--;
            }*/
				mixListLength = 0;
				currentTowerCnt[num]++;
				currentMixCnt[num]++;
			}else{
				StartCoroutine(LabelShow(" Fail!!"));
			}
		}
	}
	void BuildMenuFix(){
		//Debug.Log ("BuildMenuFix Call");
		int width=h/10;
		int height=h/10;
		
		int x=w/30;
		int y=h/10;
		
		for(int i=0; i<3; i++) GUI.Box(new Rect(w/30, h/10, (h/10+3)*7+2, h*2/3), "");
		//show up the build buttons, scrolling through currentBuildList initiated whevenever the menu is first brought up
		UnitTower[] towerList=BuildManager.GetTowerList();
		
		x+=3;	y+=3;
		
		int cnt = 0;
		
		for(int num=0; num<currentTowerListLength; num++){
			//	Debug.Log("babasda");
			//	int ID=currentBuildList[num];
			
			UnitTower tower=towerList[currentTowerList[num]];
			GUIContent guiContent; 
			if (currentTowerCnt[num] > 0){
				guiContent=new GUIContent(tower.icon, num.ToString()); 
			}else{
				if (num < 6){
					guiContent=new GUIContent((Texture)Resources.Load("Title&Icon/BTowerIcon"), num.ToString()); 
				}else{
					guiContent=new GUIContent((Texture)Resources.Load("Title&Icon/BTowerIcon" + num.ToString()), num.ToString()); 
				}
			}
			if(GUI.Button(new Rect(x, y, width, height), guiContent)){
				//if building was successful, break the loop can close the panel
				//if(BuildButtonPressed(tower)) return;
				if (currentTowerCnt[num] > 0){
					currentTowerCnt[num]--;
					tower.buildID = currentTowerList[num];
					if(BuildButtonPressed(tower)){
						return;
					}
					
				}
			}
			GUI.Label(new Rect(x, y+height-h/25, h/10, h/20), "<size=20>x" + currentTowerCnt[num].ToString() + "</size>" );
			x+=width+3;
			if ((cnt % 7)==6){
				x = w/30 + 3;
				y += height + 3;
			}
			cnt++;
		}
	}
	
	//show tooptip when a build button is hovered
	void ShowToolTip(int ID){
		//get the tower component first
		UnitTower[] towerList=BuildManager.GetTowerList();
		UnitTower tower=towerList[ID];
		
		//create a new GUIStyle to highlight the tower name with different font size and color
		GUIStyle tempGUIStyle=new GUIStyle();
		
		tempGUIStyle.fontStyle=FontStyle.Bold;
		tempGUIStyle.fontSize=h/30;
		
		//create the GUIContent that shows the tower's name
		string towerName=tower.unitName;
		GUIContent guiContentTitle=new GUIContent(towerName); 
		
		//calculate the height required
		float heightT=tempGUIStyle.CalcHeight(guiContentTitle, w/6);
		
		//switch to normal guiStyle and calculate the height needed to display cost
		/*
		tempGUIStyle.fontStyle=FontStyle.Normal;
		int[] cost=tower.GetCost();
		float heightC=0;	
		for(int i=0; i<cost.Length; i++){
			if(cost[i]>0){
				heightC+=25;
			}
		}
		*/
		//create a guiContent showing the tower description
		string towerDesp = "";
		int cnt = 0;
		for (int i =0; i<towerList.Length; i++){
			if (fusion[ID, i] != 0){ 
				towerDesp = "---Fusion---\n";
				cnt = 1;
				break;
			}
		}
		if (cnt != 0){
			for (int i = 0; i<towerList.Length; i++){
				if (fusion[ID, i] != 0){
					towerDesp += towerList[i].unitName + " x" + fusion[ID, i].ToString() + "\n";
				}
			}
			towerDesp += "-------------\n";
		}



		towerDesp +=tower.GetDescription();

		int fs = h/25;
		string fs_ = fs.ToString ();
		GUIContent guiContent=new GUIContent("<size=" + fs_ +">"  +towerDesp +"</size>"); 
		//calculate the height require to show the tower description
		float heightD= GUI.skin.GetStyle("Label").CalcHeight(guiContent, w/6);
		
		//sum up all the height, so the tooltip box size can be known
		float height=heightT+heightD+h/30;
		
		//set the tooltip draw position
		int y=h/10;
		int x;
		// if bag == if postooltip = true
		if (posToolTip)
			x=w-(w/100*3+h/10*6+24+w/6);
		else
			x=w-(w/100*2+h/10*5+18+w/6);
		//if this is a fixed or drag and drop mode, tooltip always appear at bottom left corner instaed of top left corner
	/*	if(buildMenuType==_BuildMenuType.Fixed || buildMode==_BuildMode.DragNDrop){
			y=(int)(h/10+10+h/10);
			x=(int)(Mathf.Floor(Input.mousePosition.x/50))*50;
		}*/

		//define the rect then draw the box
		Rect rect=new Rect(x, y, w/6, height);
		for(int i=0; i<2; i++) GUI.Box(rect, "");
		
		//display all the guiContent assigned ealier
		GUI.BeginGroup(rect);
			//show tower name, format it to different text style, size and color
			tempGUIStyle.fontStyle=FontStyle.Bold;
			tempGUIStyle.fontSize=h/20;
			tempGUIStyle.normal.textColor=new Color(1, 1, 0, 1f);
		GUI.Label(new Rect(5, 5, w/6, heightT+h/30), guiContentTitle, tempGUIStyle);
		
			//show tower's cost
			//get the resourceList from GameControl so we have all the information
			int[] rsc=tower.GetCost();
		//	GUI.Label(new Rect(5, 5+heightT, 150, 25), " - "+rsc[0].ToString()+"resource");
			//show desciption
		GUI.Label(new Rect(5, 5+heightT + 5, w/6, heightD+h/30 + 3), guiContent);
		GUI.EndGroup();
	}
	
	
	
	//for drag and drop, show all available tower
	void BuildMenuAllTowersFix(){
		UnitTower[] towerList=BuildManager.GetTowerList();
		
		int width=50;
		int height=50;
		
		int x=0;
		int y=Screen.height-height-6-(int)bottomPanelRect.height;
		
		for(int i=0; i<3; i++) GUI.Box(buildListRect, "");
		
		x+=3;	y+=3;
		
		for(int i=0; i<towerList.Length; i++){
			UnitTower tower=towerList[i];
			
			GUIContent guiContent=new GUIContent(tower.icon, i.ToString()); 
			if(GUI.Button(new Rect(x, y, width, height), guiContent)){
				BuildManager.BuildTowerDragNDrop(tower);
			}
			
			x+=width+3;
		}
	}
	
	
	void BuildMenuPie(){
		BuildableInfo currentBuildInfo=BuildManager.GetBuildInfo();
		
		//calculate the position in which the build list ui will be appear at
		Vector3 screenPos = Camera.main.WorldToScreenPoint(currentBuildInfo.position);
		
		int width=50;
		int height=50;
		
		Vector2[] piePos=GetPieMenuPos(currentBuildList.Length, screenPos, (int)1.414f*(width+height)/2);
		
		scatteredRectList=new Rect[currentBuildList.Length+1];
		
		//show up the build buttons, scrolling through currentBuildList initiated whevenever the menu is first brought up
		UnitTower[] towerList=BuildManager.GetTowerList();
		
		for(int num=0; num<currentBuildList.Length; num++){
			int ID=currentBuildList[num];
			
			if(ID>=0){
				UnitTower tower=towerList[ID];
				Vector2 point=piePos[num];
				
				scatteredRectList[num]=new Rect(point.x-width/2, Screen.height-point.y-height*0.75f, width, height);
				
				GUIContent guiContent=new GUIContent(tower.icon, ID.ToString()); 
				if(GUI.Button(scatteredRectList[num], guiContent)){
					//if building was successful, break the loop can close the panel
					if(BuildButtonPressed(tower)) return;
				}
				
			}
		}
		
		//clear buildmode button
		scatteredRectList[currentBuildList.Length]=new Rect(screenPos.x-width/2, Screen.height-screenPos.y+height*0.5f, width, height);
		
		if(GUI.Button(scatteredRectList[currentBuildList.Length], "X")){
			buildMenu=false;
			BuildManager.ClearBuildPoint();
			ClearBuildListRect();
		}
	}
	
	
	void BuildMenuBox(){
		BuildableInfo currentBuildInfo=BuildManager.GetBuildInfo();
		
		//calculate the position in which the build list ui will be appear at
		Vector3 screenPos = Camera.main.WorldToScreenPoint(currentBuildInfo.position);
		
		int width=50;
		int height=50;
		
		int x=(int)screenPos.x-width;
		x=Mathf.Clamp(x, 0, Screen.width-width*2);
		
		int menuLength=((int)Mathf.Floor((currentBuildList.Length+2)/2))*(height+3);
		int y=Screen.height-(int)screenPos.y;	//invert the height
		y-=menuLength/2-3;
		y=Mathf.Clamp(y, 29, Screen.height-menuLength-(int)bottomPanelRect.height);
		
		//calculate the buildlist rect
		buildListRect=new Rect(x-3, y-3, width*2+6+3, menuLength+4);
		for(int i=0; i<3; i++) GUI.Box(buildListRect, "");
		
		//show up the build buttons, scrolling through currentBuildList initiated whevenever the menu is first brought up
		UnitTower[] towerList=BuildManager.GetTowerList();
		
		for(int num=0; num<currentBuildList.Length; num++){
			int ID=currentBuildList[num];
			
			if(ID>=0){
				UnitTower tower=towerList[ID];
				
				GUIContent guiContent=new GUIContent(tower.icon, ID.ToString()); 
				if(GUI.Button(new Rect(x, y, width, height), guiContent)){
					//if building was successful, break the loop can close the panel
					if(BuildButtonPressed(tower)) return;
				}
				
				if(num%2==1){
					x-=width+3;
					y+=height+3;
				}
				else x+=width+3;
			}
		}
		
		//clear buildmode button
		if(GUI.Button(new Rect(x, y, width, height), "X")){
			buildMenu=false;
			BuildManager.ClearBuildPoint();
			ClearBuildListRect();
		}
	}
	
	
	
	
	private bool BuildButtonPressed(UnitTower tower){
		if(BuildManager.BuildTowerPointNBuild(tower)==""){
			//built, clear the build menu flag
			buildMenu=false;
			ClearBuildListRect();
			return true;
		}	
		else{
			//Debug.Log("build failed. invalide position");
			return false;
		}
	}
	
	
	//show to draw the selected tower info panel, which include the upgrade and sell button
	void SelectedTowerUI(){
		float ww = (float)w;
		float hh = (float)h;

		//Debug.Log ("aaaaaaaaaaaaaaaaaa");
		float startX=Screen.width-w/3-w/30;
		float startY=h - hh*3/4- h/ 10;
		float widthBox=ww/3 + w/60;
		float heightBox=hh * 3/4 ;
		
		towerUIRect=new Rect(startX, startY, widthBox, heightBox);
		for(int i=0; i<3; i++) GUI.Box(towerUIRect, "");
		
		startX+=w/60;
		startY+=h/60;
		
		float width=w/3;
		float height=h/30;
		
		GUIStyle tempGUIStyle=new GUIStyle();
		
		tempGUIStyle.fontStyle=FontStyle.Bold;
		tempGUIStyle.fontSize=h/25;
		tempGUIStyle.normal.textColor=new Color(1, 1, 0, 1f);
		
		string towerName=currentSelectedTower.unitName;
		GUIContent guiContentTitle=new GUIContent(towerName); 
		
		GUI.Label(new Rect(startX, startY, width, height), guiContentTitle, tempGUIStyle);
		
		tempGUIStyle.fontStyle=FontStyle.Bold;
		tempGUIStyle.fontSize=h/35;
		tempGUIStyle.normal.textColor=new Color(1, 0, 0, 1f);
		
		GUI.Label(new Rect(startX, startY+=2*height, width, height), "Level: "+currentSelectedTower.GetLevel().ToString(), tempGUIStyle);
		
		startY+=h/60;
		
		
		string towerInfo="";
		
		_TowerType type=currentSelectedTower.type;
		
		//display relevent information based on tower type
		if(type==_TowerType.SupportTower){
			
			//show buff info only
			BuffStat buffInfo=currentSelectedTower.GetBuff();
			
			string buff="";
			if(buffInfo.damageBuff!=0) buff+="Buff damage by "+(buffInfo.damageBuff*100).ToString("f1")+"%\n";
			if(buffInfo.rangeBuff!=0) buff+="Buff range by "+(buffInfo.rangeBuff*100).ToString("f1")+"%\n";
			if(buffInfo.cooldownBuff!=0) buff+="Reduce CD by "+(buffInfo.cooldownBuff*100).ToString("f1")+"%\n";
			if(buffInfo.regenHP!=0) buff+="Renegerate HP by "+(buffInfo.regenHP).ToString("f1")+" per seconds\n";
			
			towerInfo+=buff;
			
		}
		else if(type==_TowerType.TurretTower || type==_TowerType.AOETower){
			
			//show the basic info for turret and aoeTower
			if(currentSelectedTower.GetDamage()>0)
				towerInfo+="Damage: "+currentSelectedTower.GetDamage().ToString("f1")+"\n";
			if(currentSelectedTower.GetCooldown()>0)
				towerInfo+="Cooldown: "+currentSelectedTower.GetCooldown().ToString("f1")+"sec\n";
			if(type==_TowerType.TurretTower && currentSelectedTower.GetAoeRadius()>0)
				towerInfo+="AOE Radius: "+currentSelectedTower.GetAoeRadius().ToString("f1")+"\n";
			if(currentSelectedTower.GetStunDuration()>0)
				towerInfo+="Stun target for "+currentSelectedTower.GetStunDuration().ToString("f1")+"sec\n";
			
			//if the tower have damage over time value, display it
			Dot dot=currentSelectedTower.GetDot();
			float totalDot=dot.damage*(dot.duration/dot.interval);
			if(totalDot>0){
				string dotInfo="Cause "+totalDot.ToString("f1")+" damage over the next "+dot.duration+" sec\n";
				
				towerInfo+=dotInfo;
			}
			
			//if the tower have slow value, display it
			Slow slow=currentSelectedTower.GetSlow();
			if(slow.duration>0){
				string slowInfo="Slow target by "+(slow.slowFactor*100).ToString("f1")+"% for "+slow.duration.ToString("f1")+"sec\n";
				towerInfo+=slowInfo;
			}
		}
		
		
		//show the tower's description
		//towerInfo+="\n\n"+currentSelectedTower.description;
		float fsize = hh / 30;
		string fsize_ = fsize.ToString ();
		GUIContent towerInfoContent=new GUIContent(fixSize(towerInfo, fsize_));
		
		
		//draw all the information on screen
		float contentHeight= GUI.skin.GetStyle("Label").CalcHeight(towerInfoContent, 200);
		GUI.Label(new Rect(startX, startY+=20, width, h), towerInfoContent);
		
		
		//reset the draw position
		startY=h - hh*3/4- h/ 10 +hh/2;
		if(enableTargetPrioritySwitch){
			if(currentSelectedTower.type==_TowerType.TurretTower){
				//~ if(currentSelectedTower.targetingArea!=_TargetingArea.StraightLine){
				GUI.Label(new Rect(startX, startY, w, h), fixSize("Targeting Priority:", fsize_));
				if(GUI.Button(new Rect(startX+w/9*2, startY-3, w/9-w/60, h/15), fixSize(currentSelectedTower.targetPriority.ToString(), fsize_))){
					if(currentSelectedTower.targetPriority==_TargetPriority.Nearest) currentSelectedTower.SetTargetPriority(1);
					else if(currentSelectedTower.targetPriority==_TargetPriority.Weakest) currentSelectedTower.SetTargetPriority(2);
					else if(currentSelectedTower.targetPriority==_TargetPriority.Toughest) currentSelectedTower.SetTargetPriority(3);
					else if(currentSelectedTower.targetPriority==_TargetPriority.Random) currentSelectedTower.SetTargetPriority(0);
				}
				startY+=30;
				//~ }
			}
		}
		
		
		//check if the tower can be upgrade
		bool upgradable=false;
		if(!currentSelectedTower.IsLevelCapped() && currentSelectedTower.IsBuilt()){
			upgradable=true;
		}
		
		//reset the draw position
		startY=Screen.height-2 * bottomPanelRect.height - h/20;
		fsize = h / 20;
		fsize_ = fsize.ToString ();
		//if the tower is eligible to upgrade, draw the upgrade button
		if(upgradable){
			if(GUI.Button(new Rect(startX, startY, w/9-w/90, h/10), new GUIContent(fixSize("Up", fsize_), "1"))){
				//upgrade the tower, if false is return, player have insufficient fund
				if(!currentSelectedTower.Upgrade()) Debug.Log("Insufficient Resource");
			}
		}
		//sell button
		if(currentSelectedTower.IsBuilt()){
			if(GUI.Button(new Rect(startX+w/9, startY, w/9-w/90, h/10), new GUIContent(fixSize("Sell", fsize_), "2"))){
				int ratio = Random.Range(0, 10);
				if (ratio > 4){
					sellV = 0;
					//	Debug.Log ("selling!@!!!");
					//	UI.sellV = 0;
				}else{
					sellV = 1;
					//	Debug.Log ("selling!@!!!");
					//	UI.sellV = 1;
				}

				currentSelectedTower.Sell(sellV);
				if (sellV == 1){
					Debug.Log ("sellV : "+sellV);
					StartCoroutine(LabelShow(" You Get 1 Gold"));
				}else{
					Debug.Log ("sellV : " + sellV);
					StartCoroutine(LabelShow(" Fail!!"));
				}
			}
			
			if(GUI.Button(new Rect(startX+2*w/9, startY, w/9-w/90, h/10), new GUIContent(fixSize("Bag", fsize_), "3"))){
				currentMixCnt[currentSelectedTower.buildID]++;
				currentSelectedTower.Delete();
			}
		}
		
		
		
		//if the cursor is hover on the upgrade button, show the cost
		if(GUI.tooltip=="1"){
			
			int[] cost=currentSelectedTower.GetCost();
			GUI.Label(new Rect(startX+10, startY, 150, 25), " - "+cost[0].ToString()+"resource");
			
		}
		//if the cursor is hover on the sell button, show the resource gain
		else if(GUI.tooltip=="2"){
			//int[] sellValue=currentSelectedTower.GetTowerSellValue();
			//GUI.Label(new Rect(startX+120, startY, 150, 25), " + "+sellValue[0].ToString()+"resource");
		}
	}
	
	
	//called whevenever the build list is called up
	//compute the number of tower that can be build in this build pointer
	//store the tower that can be build in an array of number that reference to the towerlist
	//this is so these dont need to be calculated in every frame in OnGUI()
	void UpdateBuildList(){
		
		//get the current buildinfo in buildmanager
		BuildableInfo currentBuildInfo=BuildManager.GetBuildInfo();
		
		//get the current tower list in buildmanager
		UnitTower[] towerList=BuildManager.GetTowerList();
		
		//construct a temporary interger array the length of the buildinfo
		int[] tempBuildList=new int[towerList.Length];
		//for(int i=0; i<currentBuildList.Length; i++) tempBuildList[i]=-1;
		
		//scan through the towerlist, if the tower matched the build type, 
		//put the tower ID in the towerlist into the interger array
		int count=0;	//a number to record how many towers that can be build
		for(int i=0; i<towerList.Length; i++){
			UnitTower tower=towerList[i];
			
			if(currentBuildInfo.specialBuildableID!=null && currentBuildInfo.specialBuildableID.Length>0){
				foreach(int specialBuildableID in currentBuildInfo.specialBuildableID){
					if(specialBuildableID==tower.specialID){
						count+=1;
						break;
					}
				}
			}
			else{
				if(tower.specialID<0){
					//check if this type of tower can be build on this platform
					foreach(_TowerType type in currentBuildInfo.buildableType){
						if(tower.type==type && tower.specialID<0){
							tempBuildList[count]=i;
							count+=1;
							break;
						}
					}
				}
			}
		}
		
		//for as long as the number that can be build, copy from the temp buildList to the real buildList
		currentBuildList=new int[count];
		for(int i=0; i<currentBuildList.Length; i++) currentBuildList[i]=tempBuildList[i];
	}
	
	void OnDrawGizmos(){
		if(buildMenu){
			//BuildableInfo currentBuildInfo=BuildManager.GetBuildInfo();
			//float gridSize=BuildManager.GetGridSize();
			//Gizmos.DrawCube(currentBuildInfo.position, new Vector3(gridSize, 0, gridSize));
		}
	}
	
	
}
