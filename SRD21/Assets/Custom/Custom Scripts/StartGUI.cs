﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

public class StartGUI : MonoBehaviour {
	public static string id = "";
	public static string pwd = "";
	int w = Screen.width;
	int h = Screen.height;

	public GUISkin skin;
	
	public WWW GET(string url){
		WWW www = new WWW (url);
		StartCoroutine(WaitForRequest(www));
		return www;
	}
	
	private IEnumerator WaitForRequest(WWW www){
		yield return www;
		if (www.error == null){
			//Debug.Log ("WWW Ok!: " + www.text);
			if (www.text.Equals(id)){
				Application.LoadLevel ("StageSceneNew");
			}
		}else{
			//Debug.Log ("WWW Error: " + www.error);
		}
	}
	// Use this for initialization
	void Start () {
		if (h > w){
			int temp = h;
			h = w;
			w = temp;
		}

		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}

	void OnGUI() {
		GUI.skin = skin;

		GUIStyle tempGUIStyle=new GUIStyle();
		
		tempGUIStyle.fontStyle=FontStyle.Bold;
		tempGUIStyle.fontSize=h/10;
		tempGUIStyle.alignment = TextAnchor.UpperCenter;
		tempGUIStyle.font = GUI.skin.button.font;

		GUI.Label(new Rect(0, h/4+10, w, h), "<size=" + (h/10).ToString() + "><color=black>Tower Random Defense</color></size>", tempGUIStyle);
		GUI.Label(new Rect(0, h/4, w, h), "<size=" + (h/10).ToString() + "><color=yellow>Tower</color> <color=red>Random</color> <color=white>Defense</color></size>", tempGUIStyle);

		tempGUIStyle.fontSize = h/25;
		tempGUIStyle.alignment = TextAnchor.UpperRight;

		GUI.Label(new Rect(-w/30, h/2+h/10, w, h), "<color=white>Enter Name</color>", tempGUIStyle);
		id = GUI.TextArea (new Rect (w-w/9-w/30, h/2+h/10*3/2,  w/9, h/20), id);	

		GUI.Label(new Rect(-w/30, h/2+h/10*2, w, h), "<color=white>Enter Password</color>", tempGUIStyle);
		pwd = GUI.PasswordField(new Rect (w-w/9-w/30, h/2+h/10*5/2,  w/9, h/20), pwd, "*"[0]);	


		if(GUI.Button( new Rect (w-w/9-w/30, h/2+h/10*3 +5, w/9, h/12), "START")) {
			WWW www = GET("http://bit.sparcs.org:20001/login/" + id + "/" + pwd);
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
	}
}
