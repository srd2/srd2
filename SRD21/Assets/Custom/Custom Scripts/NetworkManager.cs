﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

public class NetworkManager : MonoBehaviour {
	public string server = "http://bit.sparcs.org:20001/";

	public WWW GET(string url, int method){
		WWW www = new WWW (url);
		if (method == 1) 
		{
			StartCoroutine (WaitForClear(www));
		}
		return www;
	}
	
	private IEnumerator WaitForClear(WWW www){
		yield return www;
		if (www.error == null){
			Debug.Log ("WWW Ok!: " + www.text);
		}else{
			Debug.Log ("WWW Error: " + www.error);
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
