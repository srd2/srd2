﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

public class StageGUINew : MonoBehaviour {
	
	public static int lastClear = 0;
	public static int stage;
	public GUISkin skin;

	public static float[] timePassed = new float[5];
	public static bool[] startGUI = new bool[5];

	public WWW GET(string url){
		WWW www = new WWW (url);
		StartCoroutine(WaitForRequest(www));
		return www;
	}
	
	private IEnumerator WaitForRequest(WWW www){
		yield return www;
		if (www.error == null){
			//Debug.Log ("WWW Ok!: " + www.text);
			lastClear = System.Convert.ToInt32(www.text);
		}else{
			lastClear = 0;
			//Debug.Log ("WWW Error: " + www.error);
		}
	}

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		GET ("http://bit.sparcs.org:20001/lastClear/" + StartGUI.id);

		for(int j=0; j<5; j++){
			timePassed[j] = 0.0f;
			startGUI[j] = false;
		}
	}
	
	void OnGUI(){
		GUI.skin = skin;
		int totalStageNumber = 18;
		
		int SCHeight = Screen.height, SCWidth = Screen.width;
		if (SCHeight > SCWidth){
			int temp = SCHeight;
			SCHeight = SCWidth;
			SCWidth = temp;
		}
		
		for(int i=0; i<4; i++){

			float timeInterval = 1.5f;

			string areastr = "";

			switch(i){
			case 0: areastr = "1"; break;
			case 1: areastr = "2"; break;
			default : areastr = (i+1).ToString(); break;
			}


			if( GUI.Button (new Rect (SCWidth/60, SCHeight/25 + SCHeight*6*i/25, SCWidth/24, SCHeight/5), areastr)){
				timePassed[i] = 0;
				startGUI[i] = true;
			}
			for(int j=0; j<5; j++){

				int stageNumber = 5*i+j;
				if( stageNumber >= totalStageNumber)
					break;

				string s = "stage "+(i+1).ToString()+"-"+(j+1).ToString();
				if (lastClear >= stageNumber+1){
					s+="\n<color=red>Clear!</color>";
				}

				stage = stageNumber+1;
				if (lastClear+1 < stage){
					GUIContent guiContent=new GUIContent((Texture)Resources.Load("Title&Icon/" + "lock") );
					GUI.Label ( new Rect ((float)(SCWidth - timePassed[i]/timeInterval*SCWidth*37/40 + j*SCWidth*37/200), SCHeight/25 + SCHeight/30 + SCHeight*6*i/25,  SCWidth/8, SCHeight/6), guiContent);
				}

				if ( GUI.Button (new Rect ((float)(SCWidth - timePassed[i]/timeInterval*SCWidth*37/40 + j*SCWidth*37/200), SCHeight/25 + SCHeight*6*i/25, SCWidth*4/25, SCHeight/5), s)){
					stage = stageNumber+1;
					if (lastClear+1 == stage){
						Application.LoadLevel ("GameScene" + (i+1).ToString() + "-" + (j+1).ToString());
					}
				}
			}
			if (startGUI[i]){
				timePassed[i] += Time.deltaTime;
				if (timePassed[i] > timeInterval){
					timePassed[i] = timeInterval;
					startGUI[i] = false;
				}
			}
		}


		GUIContent back=new GUIContent((Texture)Resources.Load("Title&Icon/" + "back"));
		if(GUI.Button(new Rect(SCWidth-SCHeight/11, SCHeight*10/11, SCHeight/12, SCHeight/12), back)){
			Application.LoadLevel("StartScene");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("StartScene"); }
		
	}
}
