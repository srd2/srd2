﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

public class StageGUI : MonoBehaviour {

	public static int lastClear = 0;
	public static int stage;
	public GUISkin skin;

	public WWW GET(string url){
		WWW www = new WWW (url);
		StartCoroutine(WaitForRequest(www));
		return www;
	}
	
	private IEnumerator WaitForRequest(WWW www){
		yield return www;
		if (www.error == null){
			///Debug.Log ("WWW Ok!: " + www.text);
			lastClear = System.Convert.ToInt32(www.text);
		}else{
			lastClear = 0;
			//Debug.Log ("WWW Error: " + www.error);
		}
	}

	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		GET ("http://bit.sparcs.org:20001/lastClear/" + StartGUI.id);
	}

	void OnGUI(){
		GUI.skin = skin;
		int totalStageNumber = 18;

		int SCHeight = Screen.height, SCWidth = Screen.width;
		if (SCHeight > SCWidth){
			int temp = SCHeight;
			SCHeight = SCWidth;
			SCWidth = temp;
		}

		for (int i=0; i<totalStageNumber/4; i++){
			for(int j=0; j<5; j++){

				int stageNumber = 5*i+j;

				if( stageNumber >= totalStageNumber)
					break;

				//Debug.Log (stageNumber);
				string s = "Stage " + (stageNumber + 1).ToString();
				if (lastClear >= stageNumber+1){
					s+="\n<color=red>Clear!</color>";
				}

				stage = stageNumber+1;
				if (lastClear+1 < stage){
					GUIContent guiContent=new GUIContent((Texture)Resources.Load("Title&Icon/" + "lock") );
					GUI.Label ( new Rect (SCWidth/60 + SCWidth/5*j +(SCWidth/6 - SCWidth/8)/2, SCHeight/40 + SCHeight/4*i,  SCWidth/8, SCHeight/5), guiContent);
				}

				if ( GUI.Button ( new Rect (SCWidth/60 + SCWidth/5*j , SCHeight/40 + SCHeight/4*i, SCWidth/6, SCHeight/5), s )) {
					stage = stageNumber+1;
					if (lastClear+1 >= stage){
							Application.LoadLevel ("GameScene");
					}

				}



			}
		}

		GUIContent back=new GUIContent((Texture)Resources.Load("Title&Icon/" + "back"));
		if(GUI.Button(new Rect(SCWidth-SCHeight/11, SCHeight*10/11, SCHeight/12, SCHeight/12), back)){
			Application.LoadLevel("StartScene");
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("StartScene"); }
	
	}
}
