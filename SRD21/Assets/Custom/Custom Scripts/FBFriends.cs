﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FBFriends : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private static List<object>                 friends         = null;
	private static Dictionary<string, string>   profile         = null;
	public int friendCounter = 0;
	public Dictionary<string, object> friend = null;

	void APICallback(FBResult result)
	{
		Util.Log("APICallback");
		if (result.Error != null)
		{
			Util.LogError(result.Error);
			// Let's just try again
			FB.API("/me?fields=id,first_name,friends.limit(100).fields(first_name,id)", Facebook.HttpMethod.GET, APICallback);
			return;
		}
		
		profile = Util.DeserializeJSONProfile(result.Text);
		//GameStateManager.Username = profile["first_name"];
		friends = Util.DeserializeJSONFriends(result.Text);
	}

	#region FB.Init() example
	
	private bool isInit = false;
	
	private void CallFBInit()
	{
		FB.Init(OnInitComplete, OnHideUnity);
	}
	
	private void OnInitComplete()
	{
		Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
		isInit = true;
	}
	
	private void OnHideUnity(bool isGameShown)
	{
		Debug.Log("Is game showing? " + isGameShown);
	}
	
	#endregion

	#region FB.Login() example
	
	private void CallFBLogin()
	{
		FB.Login("email,publish_actions", LoginCallback);
	}
	
	void LoginCallback(FBResult result)
	{
		if (result.Error != null)
			lastResponse = "Error Response:\n" + result.Error;
		else if (!FB.IsLoggedIn)
		{
			lastResponse = "Login cancelled by Player";
		}
		else
		{
			//lastResponse = "Login was successful!";
			OnLoggedIn();
		}
	}

	void OnLoggedIn()
	{
		Util.Log("Logged in. ID: " + FB.UserId);

		// Reqest player info and profile picture
		FB.API("/me?fields=id,first_name,friends.limit(10).fields(first_name,id)", Facebook.HttpMethod.GET, APICallback);
		//LoadPicture(Util.GetPictureURL("me", 128, 128),MyPictureCallback);
	}
	
	private void CallFBLogout()
	{
		FB.Logout();
	}
	#endregion

	#region GUI

	private string status = "Ready";
	private string lastResponse = "";
	
	int buttonHeight = 24;
	int mainWindowWidth = 500;
	int mainWindowFullWidth = 530;

	private bool Button(string label)
	{
		return GUILayout.Button(
			label, 
			GUILayout.MinHeight(buttonHeight), 
			GUILayout.MaxWidth(mainWindowWidth)
			);
	}

	void OnGUI()
	{
		if(Button ("FB.init")){
			CallFBInit();
		}

		GUI.enabled = isInit;
		if (Button("Login"))
		{
			CallFBLogin();
			status = "Login called";
		}

		if (Button ("Friends Start!"))
		{
			Debug.Log (friendCounter);
			if(friends == null){
				Debug.Log ("fuck");
				return;
			}
			friend = (Dictionary<string, object>) friends[friendCounter];
			friendCounter++;
			GUI.Box (new Rect (Screen.width/2, Screen.height/2, 20, 20), (string)friend["id"]);
		}
	}

	#endregion
}
